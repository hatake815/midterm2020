create table if not exists person
(
	id serial not null,
	firstname varchar not null,
	lastname varchar not null,
	city varchar not null,
	phone varchar not null,
	telegram varchar not null
);

INSERT INTO public.person (id, firstname, lastname, city, phone, telegram) VALUES (1, 'Ahmad ', 'Bernard', 'Helena Flats', '(804)548-8186', 'invitepost');
INSERT INTO public.person (id, firstname, lastname, city, phone, telegram) VALUES (2, 'Oliwia ', 'North', 'Mullin', '(313)849-6220', 'jalopykuwaiti');
INSERT INTO public.person (id, firstname, lastname, city, phone, telegram) VALUES (3, 'Guy ', 'Stein', 'Bigler', '(430)977-1519', 'geltrembling');
INSERT INTO public.person (id, firstname, lastname, city, phone, telegram) VALUES (4, 'Luca ', 'Sloan', 'Lordsburg', '(561)362-3698', 'butlerpainter');
INSERT INTO public.person (id, firstname, lastname, city, phone, telegram) VALUES (5, 'Matei', 'Graves', 'Garretts Mill', '(706)653-4453', 'tubbyrejoice');
INSERT INTO public.person (id, firstname, lastname, city, phone, telegram) VALUES (6, 'Jameel ', 'Robertson', 'Buies Creek', '(312)748-8523', 'creepberry');
INSERT INTO public.person (id, firstname, lastname, city, phone, telegram) VALUES (7, 'Karson ', 'Read', 'Dakota Dunes', '(559)310-4364', 'impedepossessive');
INSERT INTO public.person (id, firstname, lastname, city, phone, telegram) VALUES (8, 'Zakariyya ', 'Williamson', 'Mammoth', '(212)965-8580', 'terriblesniveling');
INSERT INTO public.person (id, firstname, lastname, city, phone, telegram) VALUES (9, 'Jamel ', 'Lin', 'Silas', '(620)719-3974', 'galadrieldial');
INSERT INTO public.person (id, firstname, lastname, city, phone, telegram) VALUES (10, 'Margaux ', 'Brock', 'Murray Hill', '(317)304-9316', 'talldolphin');