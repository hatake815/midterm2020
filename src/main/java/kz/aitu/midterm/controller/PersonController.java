package kz.aitu.midterm.controller;

import kz.aitu.midterm.model.Person;
import kz.aitu.midterm.service.PersonService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

public class PersonController {
    private final PersonService personService;

    public PersonController(PersonService personService) {
        this.personService = personService;
    }

    @GetMapping("/api/v2/users/{id}")
    public ResponseEntity<?> getById(@PathVariable long id){
        return ResponseEntity.ok(personService.getById(id));
    }

    @GetMapping("api/v2/users/")
    public ResponseEntity<?> findAll(){
        return ResponseEntity.ok(personService.getAll());
    }

    @DeleteMapping("/api/v2/users/{id}")
    public void deleteById(@PathVariable long id){
        personService.deleteById(id);
    }

    @PostMapping("/api/v2/users")
    public ResponseEntity<?> createPerson(@RequestBody Person person){
        return ResponseEntity.ok(personService.createPerson(person));
    }
}
